#include "stdafx.h"
#include "LabelToolGUI.h"
#include <QtWidgets/QApplication>
#include <QCommandLineParser>

int main(int argc, char *argv[])
{
	std::cout << "Running GUI program..." << std::endl;

	QApplication a(argc, argv);
	QCoreApplication::setApplicationName("TSDecoder GUI");
	QCoreApplication::setApplicationVersion("1.0");

	// Add a command line parser to input the image path
	QCommandLineParser parser;
	parser.setApplicationDescription("TSDecoder GUI");
	parser.addHelpOption();
	parser.addVersionOption();
	parser.addPositionalArgument("IMG_PATH", "input image path");

	// Process the actual command line arguments given by the user
	parser.process(a);

	const QStringList args = parser.positionalArguments();

	LabelToolGUI w;
	w.show();
	if (!args.isEmpty())
		w.GetImgPath(args[0]);

	return a.exec();
}

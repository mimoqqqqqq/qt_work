#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_LabelToolGUI.h"
using json = nlohmann::json;

class LabelToolGUI : public QMainWindow
{
	Q_OBJECT

public:
	LabelToolGUI(QWidget *parent = Q_NULLPTR);
	void GetImgPath(QString);

private slots:
	void LabelInnerCorners();
	void LabelOuterCorners();

	void OpenFileDialog();
	void MoveRoi(QPoint);
	void ResizeRoi(int);
	void Decode();
private:
	Ui::LabelToolGUIClass ui;

	std::vector<cv::Point2d> originInnerCorner;
	std::vector<cv::Point2d> originOuterCorner;

	std::vector<cv::Point2d> homoCorner;
	cv::Mat inputImg;
	cv::Mat homoImg;
	cv::Mat binImg;
	cv::Mat scaledImg;
	cv::Mat roiImg;
	cv::Rect roiRect;

	double scaleRate;
	bool haveImg = false;
	bool decodeSuccess = false;
	char *binData;
	int imgSideLength;
	int codeVersion = 16;
	std::string IdPath;

	int labelId = 0;
	int ImageId = 0;
	bool firstImg = true;

	json root;
	json image;
	json partner;

};

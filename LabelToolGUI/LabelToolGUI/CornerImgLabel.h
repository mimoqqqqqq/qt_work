#pragma once

#include <opencv2/opencv.hpp>
#include <QLabel>
#include <QPoint>
#include <QDebug>
#include <QPainter>
#include <QMouseEvent>
#include <QWheelEvent>

class CornerImgLabel : public QLabel
{
	Q_OBJECT

public:
	CornerImgLabel(QWidget *parent = Q_NULLPTR);
	void paintEvent(QPaintEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void wheelEvent(QWheelEvent *event);
	void enterEvent(QEvent *event);
	void leaveEvent(QEvent *event);
	QPoint* GetCorner();

	QPoint laseMiddleMouseButtonPos = QPoint(0, 0);
	QPoint lastRightMouseButtonPos = QPoint(0, 0);

	QPoint corner[4] = { QPoint(100, 100), QPoint(500, 100), QPoint(500, 500), QPoint{100, 500} };

	bool isLeftMouseButtonPressing = false;
	bool isMiddleMouseButtonPressing = false;
	bool isRightMouseButtonPressing = false;
	bool isMouseHovering = false;

	int maxDistId_X = 0;
	int minDistId_X = 0;
	int maxDistId_Y = 0;
	int minDistId_Y = 0;
	double tmpDistX_Max = corner[0].x();
	double tmpDistX_Min = corner[0].x();
	double tmpDistY_Max = corner[0].y();
	double tmpDistY_Min = corner[0].y();

signals:
	void middleMouseButtonMoveEvent(QPoint);
	void middleMouseWheelScrollEvent(int);
};
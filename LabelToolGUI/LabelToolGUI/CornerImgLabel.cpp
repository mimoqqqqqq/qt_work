#include "stdafx.h"
#include "CornerImgLabel.h"


CornerImgLabel::CornerImgLabel(QWidget *parent) : QLabel(parent) {}

void CornerImgLabel::paintEvent(QPaintEvent *event)
{
	QLabel::paintEvent(event);
	if (pixmap())
	{
		QPainter painter(this);
		painter.setPen(QPen(Qt::cyan, 3));
		painter.setBrush(QBrush(Qt::NoBrush));
		painter.drawConvexPolygon(corner, 4);
		painter.setPen(QPen(Qt::NoPen));
		painter.setBrush(QBrush(Qt::blue));
		for (int i = 0; i < 4; i++)
			painter.drawEllipse(corner[i], 4, 4);
	}
}

void CornerImgLabel::mouseMoveEvent(QMouseEvent *event)
{
	if (!pixmap()) return;
	if (isLeftMouseButtonPressing)
	{
		int closestCornerIdx;
		double minDist = DBL_MAX;
		for (int i = 0; i < 4; i++)
		{
			double tmpDist = (corner[i] - event->pos()).manhattanLength();
			if (tmpDist < minDist)
			{
				minDist = tmpDist;
				closestCornerIdx = i;
			}
		}

		corner[closestCornerIdx] = event->pos();

		if (corner[closestCornerIdx].x() < 0)
			corner[closestCornerIdx].setX(0);
		if (corner[closestCornerIdx].x() > 600)
			corner[closestCornerIdx].setX(600);
		if (corner[closestCornerIdx].y() < 0)
			corner[closestCornerIdx].setY(0);
		if (corner[closestCornerIdx].y() > 600)
			corner[closestCornerIdx].setY(600);

		repaint();
	}

	if (isMiddleMouseButtonPressing)
	{
		QPoint posDelta = laseMiddleMouseButtonPos - event->pos();
		laseMiddleMouseButtonPos = event->pos();
		emit middleMouseButtonMoveEvent(posDelta);
	}

	if (isRightMouseButtonPressing)
	{
		maxDistId_X = 0;
		tmpDistX_Max = corner[0].x();
		minDistId_X = 0;
		tmpDistX_Min = corner[0].x();
		maxDistId_Y = 0;
		tmpDistY_Max = corner[0].y();
		minDistId_Y = 0;
		tmpDistY_Min = corner[0].y();

		for (int i = 1; i < 4; i++)
		{
			if (tmpDistX_Max < corner[i].x())
			{
				tmpDistX_Max = corner[i].x();
				maxDistId_X = i;
			}
			if (tmpDistX_Min > corner[i].x())
			{
				tmpDistX_Min = corner[i].x();
				minDistId_X = i;
			}

			if (tmpDistY_Max < corner[i].y())
			{
				tmpDistY_Max = corner[i].y();
				maxDistId_Y = i;
			}
			if (tmpDistY_Min > corner[i].y())
			{
				tmpDistY_Min = corner[i].y();
				minDistId_Y = i;
			}
		}


		if (event->pos().x() < corner[maxDistId_X].x() && event->pos().x() > corner[minDistId_X].x())
		{
			if (event->pos().y() < corner[maxDistId_Y].y() && event->pos().y() > corner[minDistId_Y].y()) {
				for (int i = 0; i < 4; i++) {
					double tmpDistX = 0;
					tmpDistX = event->pos().x() - lastRightMouseButtonPos.x();
					tmpDistX = tmpDistX + corner[i].x();
					corner[i].setX(tmpDistX);

					double tmpDistY = 0;
					tmpDistY = event->pos().y() - lastRightMouseButtonPos.y();
					tmpDistY = tmpDistY + corner[i].y();
					corner[i].setY(tmpDistY);
				}
			}
		}
		lastRightMouseButtonPos = event->pos();
		repaint();
	}

}

void CornerImgLabel::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		isLeftMouseButtonPressing = true;
		isMiddleMouseButtonPressing = false;
		isRightMouseButtonPressing = false;
	}
	else if (event->button() == Qt::MiddleButton)
	{
		isLeftMouseButtonPressing = false;
		isMiddleMouseButtonPressing = true;
		isRightMouseButtonPressing = false;
		laseMiddleMouseButtonPos = event->pos();
	}
	else if (event->button() == Qt::RightButton)
	{
		isLeftMouseButtonPressing = false;
		isMiddleMouseButtonPressing = false;
		isRightMouseButtonPressing = true;
		lastRightMouseButtonPos = event->pos();
	}
	mouseMoveEvent(event);
}

void CornerImgLabel::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
		isLeftMouseButtonPressing = false;
	else if (event->button() == Qt::MiddleButton)
		isMiddleMouseButtonPressing = false;
	else if (event->button() == Qt::RightButton)
		isRightMouseButtonPressing = false;
}

void CornerImgLabel::wheelEvent(QWheelEvent *event)
{
	if (isMouseHovering)
		emit middleMouseWheelScrollEvent(event->angleDelta().y());
}

void CornerImgLabel::enterEvent(QEvent *event)
{
	isMouseHovering = true;
}

void CornerImgLabel::leaveEvent(QEvent *event)
{
	isMouseHovering = false;
}

QPoint* CornerImgLabel::GetCorner()
{
	return corner;
}

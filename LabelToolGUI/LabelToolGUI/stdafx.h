#include <QtWidgets>
#pragma execution_character_set("utf-8")

/**
*	@brief Header files for the STL library
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <experimental/filesystem>
#include <bitset>

//QT Library
#include <QtWidgets/QMainWindow>
#include <QFileDialog>
#include <QPoint>
#include <QDebug>

/**
*	@brief Header files for OpenCV library
*/
#include <opencv2/opencv.hpp>
#include <libsvm/svm.h>
#include "json.hpp"

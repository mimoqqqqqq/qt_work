/********************************************************************************
** Form generated from reading UI file 'LabelToolGUI.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LABELTOOLGUI_H
#define UI_LABELTOOLGUI_H

#include <CornerImgLabel.h>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LabelToolGUIClass
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *topHorizontalLayout;
    QLabel *fileNameLabel;
    QLineEdit *filePathLineEdit;
    QPushButton *browseButton;
    CornerImgLabel *imgLabel;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *labelPushButton_in;
    QPushButton *labelPushButton_out;
    QPushButton *decodePushButton;

    void setupUi(QMainWindow *LabelToolGUIClass)
    {
        if (LabelToolGUIClass->objectName().isEmpty())
            LabelToolGUIClass->setObjectName(QString::fromUtf8("LabelToolGUIClass"));
        LabelToolGUIClass->resize(618, 709);
        centralWidget = new QWidget(LabelToolGUIClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        centralWidget->setAutoFillBackground(false);
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        topHorizontalLayout = new QHBoxLayout();
        topHorizontalLayout->setSpacing(6);
        topHorizontalLayout->setObjectName(QString::fromUtf8("topHorizontalLayout"));
        fileNameLabel = new QLabel(centralWidget);
        fileNameLabel->setObjectName(QString::fromUtf8("fileNameLabel"));

        topHorizontalLayout->addWidget(fileNameLabel);

        filePathLineEdit = new QLineEdit(centralWidget);
        filePathLineEdit->setObjectName(QString::fromUtf8("filePathLineEdit"));
        filePathLineEdit->setReadOnly(true);

        topHorizontalLayout->addWidget(filePathLineEdit);

        browseButton = new QPushButton(centralWidget);
        browseButton->setObjectName(QString::fromUtf8("browseButton"));

        topHorizontalLayout->addWidget(browseButton);


        verticalLayout->addLayout(topHorizontalLayout);

        imgLabel = new CornerImgLabel(centralWidget);
        imgLabel->setObjectName(QString::fromUtf8("imgLabel"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(imgLabel->sizePolicy().hasHeightForWidth());
        imgLabel->setSizePolicy(sizePolicy1);
        imgLabel->setMinimumSize(QSize(600, 600));
        imgLabel->setMaximumSize(QSize(600, 600));
        imgLabel->setLayoutDirection(Qt::LeftToRight);
        imgLabel->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(imgLabel);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        labelPushButton_in = new QPushButton(centralWidget);
        labelPushButton_in->setObjectName(QString::fromUtf8("labelPushButton_in"));

        horizontalLayout_2->addWidget(labelPushButton_in);

        labelPushButton_out = new QPushButton(centralWidget);
        labelPushButton_out->setObjectName(QString::fromUtf8("labelPushButton_out"));

        horizontalLayout_2->addWidget(labelPushButton_out);


        verticalLayout->addLayout(horizontalLayout_2);

        decodePushButton = new QPushButton(centralWidget);
        decodePushButton->setObjectName(QString::fromUtf8("decodePushButton"));

        verticalLayout->addWidget(decodePushButton);

        LabelToolGUIClass->setCentralWidget(centralWidget);

        retranslateUi(LabelToolGUIClass);

        QMetaObject::connectSlotsByName(LabelToolGUIClass);
    } // setupUi

    void retranslateUi(QMainWindow *LabelToolGUIClass)
    {
        LabelToolGUIClass->setWindowTitle(QApplication::translate("LabelToolGUIClass", "LabelToolGUI", nullptr));
        fileNameLabel->setText(QApplication::translate("LabelToolGUIClass", "\345\234\226\347\211\207\350\267\257\345\276\221\357\274\232", nullptr));
        browseButton->setText(QApplication::translate("LabelToolGUIClass", "Browse...", nullptr));
        imgLabel->setText(QString());
        labelPushButton_in->setText(QApplication::translate("LabelToolGUIClass", "Label Inner", nullptr));
        labelPushButton_out->setText(QApplication::translate("LabelToolGUIClass", "Label Outer", nullptr));
        decodePushButton->setText(QApplication::translate("LabelToolGUIClass", "Generate Json File", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LabelToolGUIClass: public Ui_LabelToolGUIClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LABELTOOLGUI_H

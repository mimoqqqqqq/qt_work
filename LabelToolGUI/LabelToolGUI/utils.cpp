#include "stdafx.h"
#include "utils.h"

using namespace std;
using namespace cv;

void CreateFolder(string folderName)
{
	if (_access(folderName.c_str(), 0)) CreateDirectoryA(folderName.c_str(), NULL);
}

QImage MatToQImage(Mat const& src)
{
	Mat temp; // make the same cv::Mat
	cvtColor(src, temp, CV_BGR2RGB); // cvtColor Makes a copt, that what i need
	QImage dest((const uchar *)temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
	dest.bits(); // enforce deep copy, see documentation 
	// of QImage::QImage ( const uchar * data, int width, int height, Format format )
	return dest;
}

Mat QImageToMat(QImage const& src)
{
	Mat tmp(src.height(), src.width(), CV_8UC3, (uchar*)src.bits(), src.bytesPerLine());
	Mat result; // deep copy just in case (my lack of knowledge with open cv)
	cvtColor(tmp, result, CV_BGR2RGB);
	return result;
}

string ExecCommand(string cmd)
{
	char buffer[128];
	string result = "";
	FILE* pipe = _popen(cmd.c_str(), "r");
	if (!pipe)
	{
		_pclose(pipe);
		throw runtime_error("popen() failed!");
	}
	try
	{
		while (!feof(pipe))
		{
			if (fgets(buffer, 128, pipe) != NULL)
				result += buffer;
		}
	}
	catch (...)
	{
		_pclose(pipe);
		throw;
	}
	_pclose(pipe);

	return result;
}
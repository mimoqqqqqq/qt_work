#pragma once

#include <QImage>
#include <bitset>
#include <windows.h>
#include <opencv2/opencv.hpp>
#include <io.h>

void CreateFolder(std::string folderName);
QImage MatToQImage(cv::Mat const& src);
cv::Mat QImageToMat(QImage const& src);
std::string ExecCommand(std::string cmd);
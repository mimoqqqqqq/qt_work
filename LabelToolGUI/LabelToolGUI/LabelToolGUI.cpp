#include "stdafx.h"
#include "LabelToolGUI.h"
#include "utils.h"

using namespace std;
using namespace cv;

LabelToolGUI::LabelToolGUI(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	connect(ui.browseButton, SIGNAL(clicked()), this, SLOT(OpenFileDialog()));
	connect(ui.decodePushButton, SIGNAL(clicked()), this, SLOT(Decode()));
	connect(ui.labelPushButton_in, SIGNAL(clicked()), this, SLOT(LabelInnerCorners()));
	connect(ui.labelPushButton_out, SIGNAL(clicked()), this, SLOT(LabelOuterCorners()));

	connect(ui.imgLabel, SIGNAL(middleMouseButtonMoveEvent(QPoint)), this, SLOT(MoveRoi(QPoint)));
	connect(ui.imgLabel, SIGNAL(middleMouseWheelScrollEvent(int)), this, SLOT(ResizeRoi(int)));

}

void LabelToolGUI::GetImgPath(QString filePath)
{
	if (filePath.isEmpty() || filePath.isNull()) return;

	ui.filePathLineEdit->setText(filePath);

	inputImg = imread(filePath.toStdString(), CV_LOAD_IMAGE_COLOR);

	imgSideLength = min(inputImg.rows, inputImg.cols);
	roiRect = Rect(0, 0, 600, 600);

	scaleRate = 600.0 / imgSideLength;
	cv::resize(inputImg, scaledImg, Size(0, 0), scaleRate, scaleRate, CV_INTER_AREA);

	roiImg = scaledImg(roiRect).clone();
	ui.imgLabel->setPixmap(QPixmap::fromImage(MatToQImage(roiImg)));

	haveImg = true;
}
void LabelToolGUI::OpenFileDialog()
{
	QString filePath = QFileDialog::getOpenFileName(this, tr("開啟檔案"), "./", "支援的圖片格式 (*.bmp;*.jpg;*.jpeg;*.jpe;*.tif;*.tiff;*.png)");
	if (filePath.isEmpty() || filePath.isNull()) return;

	ui.filePathLineEdit->setText(filePath);
	if (!partner.is_null()) {
		for (int i = 0; i < labelId - 1; i++)
		{
			partner.erase(i);
		}
		labelId = 0;
		//cout << image.is_null() << endl;
	}

	QFileInfo fileInfo(filePath);
	IdPath = fileInfo.fileName().toStdString();
	image["Id"] = { IdPath };
	if (!firstImg)
		ImageId++;


	inputImg = imread(filePath.toStdString(), CV_LOAD_IMAGE_COLOR);

	imgSideLength = min(inputImg.rows, inputImg.cols);
	roiRect = Rect(0, 0, 600, 600);

	scaleRate = 600.0 / imgSideLength;
	cv::resize(inputImg, scaledImg, Size(0, 0), scaleRate, scaleRate, CV_INTER_AREA);

	roiImg = scaledImg(roiRect).clone();
	ui.imgLabel->setPixmap(QPixmap::fromImage(MatToQImage(roiImg)));

	haveImg = true;
}

void LabelToolGUI::MoveRoi(QPoint posDelta)
{
	if (!haveImg) return;

	// 防止移動時圖片超出範圍
	roiRect.x += posDelta.x();
	if (roiRect.x + 600 > scaledImg.cols) roiRect.x = scaledImg.cols - 600;
	else if (roiRect.x < 0) roiRect.x = 0;

	roiRect.y += posDelta.y();
	if (roiRect.y + 600 > scaledImg.rows) roiRect.y = scaledImg.rows - 600;
	else if (roiRect.y < 0) roiRect.y = 0;

	roiImg = scaledImg(roiRect).clone();
	ui.imgLabel->setPixmap(QPixmap::fromImage(MatToQImage(roiImg)));
}

void LabelToolGUI::ResizeRoi(int scrollDeltaY)
{
	if (!haveImg) return;

	// 限制縮放上限
	if (scrollDeltaY > 0 && scaleRate < 600.0 / imgSideLength * 4)
		scaleRate += 600.0 / imgSideLength * 0.1;
	else if (scrollDeltaY < 0 && scaleRate > 600.0 / imgSideLength)
		scaleRate -= 600.0 / imgSideLength * 0.1;

	// 防止縮放時圖片超出範圍
	if (scaleRate > 600.0 / imgSideLength * 4)
		scaleRate = 600.0 / imgSideLength * 4;
	else if (scaleRate < 600.0 / imgSideLength)
		scaleRate = 600.0 / imgSideLength;

	Size oldScaledImgSize = scaledImg.size();

	scaledImg.release();
	cv::resize(inputImg, scaledImg, Size(0, 0), scaleRate, scaleRate, CV_INTER_AREA);

	// 使Roi在縮放後的中心位置保持不變
	roiRect.x -= (oldScaledImgSize.width - scaledImg.cols) / 2.0;
	roiRect.y -= (oldScaledImgSize.height - scaledImg.rows) / 2.0;

	// 防止縮放時圖片超出範圍
	if (roiRect.x + 600 > scaledImg.cols) roiRect.x = scaledImg.cols - 600;
	else if (roiRect.x < 0) roiRect.x = 0;
	if (roiRect.y + 600 > scaledImg.rows) roiRect.y = scaledImg.rows - 600;
	else if (roiRect.y < 0) roiRect.y = 0;

	roiImg = scaledImg(roiRect).clone();
	ui.imgLabel->setPixmap(QPixmap::fromImage(MatToQImage(roiImg)));
}

void LabelToolGUI::LabelInnerCorners() {
	originInnerCorner.clear();
	originInnerCorner = vector<Point2d>(4);
	QPoint *tmpQPointCorner = ui.imgLabel->GetCorner();
	Point2d tmpPoint;
	for (int i = 0; i < 4; i++)
	{
		tmpPoint.x = (tmpQPointCorner[i].x() + roiRect.x) / scaleRate;
		tmpPoint.y = (tmpQPointCorner[i].y() + roiRect.y) / scaleRate;
		originInnerCorner[i] = tmpPoint;

	}
	

	partner = { originInnerCorner[0].x, originInnerCorner[0].y,
					 originInnerCorner[1].x, originInnerCorner[1].y,
					 originInnerCorner[2].x, originInnerCorner[2].y,
					 originInnerCorner[3].x, originInnerCorner[3].y };
	image["InnerContour"] = partner;
	root = image;


}

void LabelToolGUI::LabelOuterCorners() {
	originOuterCorner.clear();
	originOuterCorner = vector<Point2d>(4);
	QPoint *tmpQPointCorner = ui.imgLabel->GetCorner();
	Point2d tmpPoint;
	for (int i = 0; i < 4; i++)
	{
		tmpPoint.x = (tmpQPointCorner[i].x() + roiRect.x) / scaleRate;
		tmpPoint.y = (tmpQPointCorner[i].y() + roiRect.y) / scaleRate;
		originOuterCorner[i] = tmpPoint;

		//circle(inputImg, tmpPoint, 6, Scalar(0, 0, 255), -1);
	}

	partner = { originOuterCorner[0].x, originOuterCorner[0].y,
					 originOuterCorner[1].x, originOuterCorner[1].y,
					 originOuterCorner[2].x, originOuterCorner[2].y,
					 originOuterCorner[3].x, originOuterCorner[3].y };
	image["OuterCorner"] = partner;
	root = image;

}
void LabelToolGUI::Decode()
{
	if (!haveImg) return;

	CreateFolder("json");
	ofstream o("json/"+IdPath.substr(0, IdPath.size() - 4) + "_Corners.json");
	o << setw(4) << root << endl;

	/// json for detail corners
	ifstream cornerFile;
	std::string jsonCornerTest = "json/" + IdPath.substr(0, IdPath.size() - 4) + "_Corners.json";			//load json
	cornerFile.open(jsonCornerTest);

	stringstream corStream;
	corStream << cornerFile.rdbuf();															//read json
	std::string jsonCorStr = corStream.str();

	nlohmann::json cornerJson = nlohmann::json::parse(jsonCorStr);

	nlohmann::json allInnerSearchCorJson = cornerJson["OuterCorner"];		//get all search area corners (inner)
	nlohmann::json allBchCorJson = cornerJson["InnerContour"];					//get all bch area corners

	vector<Point2d> innercorners;
	vector<Point2d> outercorners;

	for (unsigned int i = 0; i < allInnerSearchCorJson.size(); i += 2)
	{

		outercorners.push_back(Point2d(allInnerSearchCorJson[i], allInnerSearchCorJson[i + 1]));
		innercorners.push_back(Point2d(allBchCorJson[i], allBchCorJson[i + 1]));
	}
	for (int i = 0; i < innercorners.size(); i++) {
		circle(inputImg, innercorners[i], 6, Scalar(0, 0, 255), -1);
		circle(inputImg, outercorners[i], 6, Scalar(255, 0, 0), -1);

	}

	CreateFolder("labelImg");
	imwrite("labelImg/"+IdPath.substr(0, IdPath.size() - 4) + "_Corners.jpg", inputImg);
	//namedWindow("CorrectTT", WINDOW_NORMAL);
	//imshow("CorrectTT", inputImg);
	//waitKey(0);
}